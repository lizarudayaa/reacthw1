import React, {Component } from 'react';
import Button from './components/Button/Button';
import Modal from './components/Modal/Modal';
import { modals } from './components/Modal/ModalDeclaration';
import styled from 'styled-components';

const BtnWrapper = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

class App extends Component {

  state={
    opendModal: null
  }

  openModal(id){
    this.setState({opendModal: id})
  }

  closeModal = () => {
    this.setState({opendModal: null})
  }

  render() {
    const modal = modals.find(i => i.modalID === this.state.opendModal)
    return (
      <div>
        <BtnWrapper>
          <Button color={'crimson'} text='Open first modal' onClick={() => this.openModal(1) } />
          <Button color={'papayawhip'} text='Open second modal' onClick={() => this.openModal(2)} />
        </BtnWrapper>
        <div>
          {modal && <Modal 
            closeModal={this.closeModal}
            header={modal.header} 
            text={modal.text}
            actions={modal.actions({
              btnClose: () => {
                this.closeModal()
              }
            })}
          />}
        </div>
    </div>
    )
  }
}



export default App;

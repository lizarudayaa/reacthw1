import React, {Component } from 'react';
import styled from 'styled-components';

const ButtonStyled = styled.button`
  font-size: 15px;
  width:150px;
  text-transform: uppercase;
  text-align: center;
  padding: 10px 20px;
  margin: 5px;
  border: none;
  border-radius: 5px;
  background : ${props => props.color}
`

class Button extends Component {
    render() {
        const {color , text , onClick} = this.props;
        return (
           <ButtonStyled color={color} onClick={onClick}>{text}</ButtonStyled>
        )
    }
}

export default Button
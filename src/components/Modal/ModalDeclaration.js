import Button from "../Button/Button"

export const modals = [
    {
        modalID:1,
        header:'Do you want to delete this file?',
        text:`Once you delete this file, it won’t be possible to undo this action.Are you sure you want to delete it?`,
        actions: function({btnClose}){
            return(
                <>
                <Button text='Ok' color='#b3382c' onClick={btnClose}  />
                <Button text='Cancel' color='#b3382c' onClick={btnClose}/>
                </>
            )
        }
    } ,
    {
        modalID:2,
        header:'You opened the second modal',
        text:'This is just some random text to show the difference between modals:)',
        actions: function({btnClose}){
            return(
                <>
                <Button color='crimson' text='Cool!' onClick={btnClose}/>
                <Button color='palevioletred' text=':/' onClick={btnClose} />
                </>
            )
        } 
    }
]
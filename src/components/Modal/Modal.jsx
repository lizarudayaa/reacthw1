import React, {Component } from 'react';
import styled from 'styled-components';

const ModalWindow = styled.div`
  width: 100vw;
  height: 100vh;
  background: rgba(0, 0, 0, 0.2);
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  justify-content: center;
  align-items: center;
`

const ModalContent = styled.div`
    max-width: 450px;
    position: absolute;   
`

const ModalHeader = styled.div`
      display: flex;
      justify-content: space-between;
      align-items: center;
      padding: 20px;
      background-color: #d44637;
`

const ModalClose = styled.span`
      font-size: 20px;
      cursor: pointer;
`
const ModalBody = styled.div`
    background: #e74c3c;
`
const ModalText = styled.div`
      background: #e74c3c;
      text-aling:center;
      line-height:20px;
      padding:30px;
`

const ModalActions= styled.div`
      display: flex;
      justify-content: center;
      align-items: center;
      padding-bottom: 10px;
`

class Modal extends Component {

    render() {
        const {header , text , actions, closeModal} = this.props
        return (
            <ModalWindow onClick={() => closeModal()}>
                <ModalContent onClick={e => e.stopPropagation()}>
                    <ModalHeader>
                        <h3>{header}</h3>
                        <ModalClose onClick={() => closeModal()}>X</ModalClose>
                    </ModalHeader>
                    <ModalBody>
                        <ModalText>
                            <p>{text}</p>
                        </ModalText>
                        <ModalActions>
                            {actions}
                        </ModalActions>
                    </ModalBody>
                </ModalContent>
            </ModalWindow>
        )
    }
}

export default Modal